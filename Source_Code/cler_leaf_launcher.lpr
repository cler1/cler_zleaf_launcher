program cler_leaf_launcher;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, Unit2, ErrorDialog
  { you can add units after this };

{$R *.res}

begin
  RequireDerivedFormResource:=True;
  Application.Scaled:=True;
  Application.Initialize;
  Application.CreateForm(Ttitle_cler_z_leaf_launcher, title_cler_z_leaf_launcher
    );
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TDuplicatedStartWarning, DuplicatedStartWarning);
  Application.Run;
end.

